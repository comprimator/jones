const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const sgMail = require('@sendgrid/mail');


const app = express();
const port = process.env.PORT || 5000;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));


/**
 * Handle POST request and send e-mail based on parameters request
 */
app.post('/api/email', (req, res) => {
  const email = req.body.email;

  // API key set
  sgMail.setApiKey(process.env.SENDGRID_API_KEY);

  // Build message body
  const msg = {
    to: email.email,
    from: 'jones@getjones.com',
    subject: 'New Lead',
    text:
      `
       First Name: ${email.firstName}
       Last Name: ${email.lastName}
       Mail Address: ${email.email}
       Phone Number: ${email.phone}`,
  };
  //Try to send e-mail
  sgMail.send(msg).then(() => {
    //send 200 OK
    res.sendStatus(200);
    console.log('message was sent');
  })
  .catch(error => {
    //handle errors
    res.sendStatus(500);
    console.error(error.toString());
  });
});

if (process.env.NODE_ENV === 'production') {
  // Serve any static files
  app.use(express.static(path.join(__dirname, 'client/build')));

  // Handle React routing, return all requests to React app
  app.get('*', function (req, res) {
    res.sendFile(path.join(__dirname, 'client/build', 'index.html'));
  });
}

app.listen(port, () => console.log(`Listening on port ${port}`));