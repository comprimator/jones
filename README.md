This application sends a single email with a data that a user provides.
The message may fall down to spam folder. Please check it if you do not see email.
 
NB! Application has client and server part as API key need to be secured. Also it is avoiding CORS issue. 

#Installation

##Git clone or just download project files: 
### `git clone https://comprimator@bitbucket.org/comprimator/jones.git`

##Install server & client running following commands in app directory:
### `cd jones`
### `npm install & cd client && npm install && cd ..`


##Install SendGrid API key: 
### `export SENDGRID_API_KEY='<YOUR_SENDGRID_API_KEY>'`
or just use my predefined key to set SendGrid API key
### `source ./sendgrid.env`

# Available Scripts

## Start application
In the project directory, you can run:
### `npm run all`

Runs the app (client & server).<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

## Start application in dev mode
### `npm run dev`
Runs the app in the development mode.<br>
NB! yarn and nodemon should be installed on your system.

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

