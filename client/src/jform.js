/**
 * Form definition component.
 */
import React from 'react';
import {Formik, Field, Form} from 'formik';
import * as Yup from 'yup';

/**
 * Validation scheme. Using for separate validation logic from a view.
 */
const FormSchema = Yup.object().shape({
  firstName: Yup.string()
  .trim()
  .required('Please enter your first name')
  .test('is-latin', 'Please use latin letters only for first name', val => /^[a-zA-Z]+$/.test(val))
  .min(2, 'First name must be at least 2 characters'),
  lastName: Yup.string()
  .trim()
  .required('Please enter your last name')
  .test('is-latin', 'Please use latin letters only for last name', val => /^[a-zA-Z]+$/.test(val))
  .min(2, 'Last name must be at least 2 characters'),
  email: Yup.string()
  .email('Please enter correct correct email address')
  .required('Please enter your e-mail address'),
  phone: Yup.string()
  .ensure()
  .trim()
  .required('Please enter your phone number (10-digits only)')
  .test('is-digits', 'Please use digits only', val => /^\d+$/.test(val))
  .test('len', 'Please enter correct phone number. It must be 10 digits.', val => val.length === 10)
});

/**
 * Takes input from user, validate form and allow to send content to specified e-mail.
 */
const FormDef = () => (
  <div>
    <Formik
      initialValues={{
        email: '',
        firstName: '',
        lastName: '',
        phone: '',
      }}
      validationSchema={FormSchema}
      onSubmit={values => {
        setTimeout(async () => {
          const response = await fetch('/api/email', {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({email: values}),
          });

          const status = response.status;
          if (status === 200) {
            alert("Email sent");
          } else {
            console.log('Something going wrong.');
          }
          console.log(`HTTP status: ${status}`);
        }, 500);
      }}

      render={({errors, touched, isValid}) => (
        <Form className="col-6">
          <div className="form-group">
            <label htmlFor="firstName">First Name</label>
            <Field className="form-control" name="firstName" placeholder="ex: Bill" type="text"/>
            {errors.firstName && touched.firstName ? (<div className="small text-danger">{errors.firstName}</div>) : null}
          </div>

          <div className="form-group">
            <label htmlFor="lastName">Last Name</label>
            <Field className="form-control" name="lastName" placeholder="Smith" type="text"/>
            {errors.lastName && touched.lastName ? (<div className="small text-danger">{errors.lastName}</div>) : null}
          </div>

          <div className="form-group">
            <label htmlFor="email">Email</label>
            <Field className="form-control" name="email" placeholder="bill@example.com" type="email"/>
            {errors.email && touched.email ? <div className="small text-danger">{errors.email}</div> : null}
          </div>

          <div className="form-group">
            <label htmlFor="phone">Phone (10-digits)</label>
            <Field className="form-control" name="phone" placeholder="0123456789" type="text"/>
            {errors.phone && touched.phone ? (<div className="small text-danger">{errors.phone}</div>) : null}
          </div>

          <div className="form-group">
            <button className="col-4 btn btn-lg btn-primary" disabled={!isValid} type="submit">Send email</button>
            {!isValid && <span className="col-2 text-info small">Please fill  all fields correctly</span>}
          </div>
        </Form>
      )}
    />
  </div>
);

export default FormDef;