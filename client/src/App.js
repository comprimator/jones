import React, {Component} from 'react';
import './App.css';
import FormDef from './jform'

class App extends Component {
  render () {
    return (
      <div>
        <div className="h2 col-6">Jones Form</div>
        <FormDef/>
      </div>
    );
  }
}

export default App;
